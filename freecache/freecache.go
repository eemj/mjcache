package freecache // import "go.jamie.mt/mjcache/freecache"

import (
	"context"
	"time"

	"github.com/coocood/freecache"
	cache "go.jamie.mt/mjcache/cacher"
)

// FreeCacheCacher is based on freecache
type FreeCacheCacher struct {
	fc *freecache.Cache
}

// BytesMany implements cache.Cacher
func (m *FreeCacheCacher) BytesMany(ctx context.Context, keys ...string) (map[string][]byte, error) {
	var result = make(map[string][]byte)
	for _, key := range keys {
		bytes, err := m.Bytes(ctx, key)
		if err != nil {
			return nil, err
		}
		if bytes == nil {
			continue
		}
		result[key] = bytes
	}
	return result, nil
}

// SetManyExp implements cache.Cacher
func (m *FreeCacheCacher) SetManyExp(ctx context.Context, items map[string][]byte, duration time.Duration) (err error) {
	for key, value := range items {
		err = m.Set(ctx, key, value, duration)
		if err != nil {
			return err
		}
	}
	return nil
}

// SetMany implements cache.Cacher
func (m *FreeCacheCacher) SetMany(ctx context.Context, items map[string][]byte) (err error) {
	for key, value := range items {
		err = m.Set(ctx, key, value, 0)
		if err != nil {
			return err
		}
	}
	return nil
}

// Bytes implements cache.Cacher
func (m *FreeCacheCacher) Bytes(_ context.Context, key string) (bytes []byte, err error) {
	bytes, _err := m.fc.Get([]byte(key))
	if _err != nil {
		if _err == freecache.ErrNotFound {
			return nil, nil
		}
		return nil, _err
	}
	return bytes, nil
}

// Close implements cache.Cacher
func (m *FreeCacheCacher) Close() (err error) {
	m.fc.Clear()
	return nil
}

// Delete implements cache.Cacher
func (m *FreeCacheCacher) Delete(_ context.Context, key string) (err error) {
	m.fc.Del([]byte(key))
	return nil
}

// DeleteMany implements cache.Cacher
func (m *FreeCacheCacher) DeleteMany(ctx context.Context, keys ...string) (err error) {
	for _, key := range keys {
		m.Delete(ctx, key)
	}
	return nil
}

// Exists implements cache.Cacher
func (m *FreeCacheCacher) Exists(_ context.Context, key string) (has bool, err error) {
	_, _err := m.fc.Get([]byte(key))
	switch _err {
	case freecache.ErrNotFound:
		return false, nil
	case nil:
		return true, nil
	default:
		return false, _err
	}
}

// Set implements cache.Cacher
func (m *FreeCacheCacher) Set(_ context.Context, key string, bytes []byte, duration time.Duration) (err error) {
	return m.fc.Set([]byte(key), bytes, int(duration.Seconds()))
}

var _ cache.Cacher = &FreeCacheCacher{}

func NewFreeCacheCacher(size int) *FreeCacheCacher {
	return &FreeCacheCacher{fc: freecache.NewCache(size)}
}
