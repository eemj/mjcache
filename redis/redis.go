package redis // import "go.jamie.mt/mjcache/redis"

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/redis/go-redis/v9"
	cache "go.jamie.mt/mjcache/cacher"
)

type RedisCacher struct {
	client redis.UniversalClient
}

// Delete implements cache.Cacher
func (c *RedisCacher) Delete(ctx context.Context, key string) (err error) {
	return c.client.Del(ctx, key).Err()
}

func (c *RedisCacher) DeleteMany(ctx context.Context, keys ...string) (err error) {
	return c.client.Del(ctx, keys...).Err()
}

// Exists implements cache.Cacher
func (c *RedisCacher) Exists(ctx context.Context, key string) (has bool, err error) {
	count, err := c.client.Exists(ctx, key).Result()
	return count > 0, err
}

// Bytes implements cache.Cacher
func (c *RedisCacher) Bytes(ctx context.Context, key string) (bytes []byte, err error) {
	bytes, err = c.client.Get(ctx, key).Bytes()
	if err == redis.Nil {
		return nil, nil
	}
	return bytes, err
}

// Set implements cache.Cacher
func (c *RedisCacher) Set(
	ctx context.Context,
	key string,
	bytes []byte,
	duration time.Duration,
) (err error) {
	_duration := duration
	if _duration <= 0 {
		_duration = redis.KeepTTL
	}
	return c.client.Set(ctx, key, bytes, _duration).Err()
}

// SetManyExp implements cache.Cacher
func (c *RedisCacher) SetManyExp(
	ctx context.Context,
	items map[string][]byte,
	duration time.Duration,
) (err error) {
	_duration := duration
	if _duration <= 0 {
		_duration = redis.KeepTTL
	}
	if _duration == redis.KeepTTL {
		return c.SetMany(ctx, items)
	}
	// Process them in a pipeline'd manner, a possible improvement is figure out
	// which is the more performant, MSET with a pipeline expiry or
	// pipeline SETEX (less commands) but the MSET is atomic.
	_, err = c.client.Pipelined(ctx, func(p redis.Pipeliner) error {
		for key, value := range items {
			if _err := p.SetEx(ctx, key, value, _duration).Err(); _err != nil {
				return _err
			}
		}
		return nil
	})
	return err
}

// SetMany implements cache.Cacher
func (c *RedisCacher) SetMany(
	ctx context.Context,
	items map[string][]byte,
) (err error) {
	var _values []interface{}
	for key, value := range items {
		_values = append(_values, []byte(key), value)
	}
	return c.client.MSet(ctx, _values...).Err()
}

// BytesMany implements cache.Cacher
func (c *RedisCacher) BytesMany(
	ctx context.Context,
	keys ...string,
) (map[string][]byte, error) {
	items, err := c.client.MGet(ctx, keys...).Result()
	if err != nil {
		return nil, err
	}
	result := make(map[string][]byte)
	for index, item := range items {
		var (
			key   = keys[index]
			value []byte
		)

		switch v := item.(type) {
		case []byte:
			value = v
		case string:
			value = []byte(v)
		case nil:
			continue
		default:
			// Iterate through the whole item list and keep appending errors
			// till we're done, we'll send the errors to the caller.
			err = errors.Join(err, fmt.Errorf("unsupported type `%T` from key `%s`", item, key))
		}

		result[key] = value
	}
	if err != nil {
		return nil, err
	}
	return result, nil
}

// Close implements cache.Cacher
func (c *RedisCacher) Close() (err error) {
	return c.client.Close()
}

// Instance returns the underlying cache interface.
func (c *RedisCacher) Instance() redis.UniversalClient {
	return c.client
}

func NewRedisCacher(client redis.UniversalClient) *RedisCacher {
	return &RedisCacher{
		client: client,
	}
}

var _ cache.Cacher = &RedisCacher{}
