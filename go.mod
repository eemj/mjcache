module go.jamie.mt/mjcache

go 1.20

require (
	github.com/coocood/freecache v1.2.3
	github.com/dgraph-io/ristretto v0.1.1
	github.com/google/uuid v1.3.0
	github.com/redis/go-redis/v9 v9.0.5
	github.com/vmihailenco/msgpack/v5 v5.3.5
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/dustin/go-humanize v1.0.1 // indirect
	github.com/golang/glog v1.1.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
	golang.org/x/sys v0.9.0 // indirect
)
