package codec

import (
	"bytes"
	"encoding/gob"
)

type GOB struct{}

func (g *GOB) MarshalBinary(d any) ([]byte, error) {
	buffer := bytes.NewBuffer(nil)
	if err := gob.NewEncoder(buffer).Encode(d); err != nil {
		return nil, err
	}
	return buffer.Bytes(), nil
}

func (g *GOB) UnmarshalBinary(data []byte, d any) (err error) {
	return gob.NewDecoder(bytes.NewReader(data)).Decode(d)
}
