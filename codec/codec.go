package codec

type Codec interface {
	MarshalBinary(d any) (bytes []byte, err error)
	UnmarshalBinary(data []byte, d any) (err error)
}
