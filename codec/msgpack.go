package codec

import (
	"github.com/vmihailenco/msgpack/v5"
)

type MsgPack struct{}

func (m *MsgPack) MarshalBinary(d any) (bytes []byte, err error) {
	return msgpack.Marshal(d)
}

func (m *MsgPack) UnmarshalBinary(data []byte, d any) (err error) {
	return msgpack.Unmarshal(data, d)
}
