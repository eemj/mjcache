package codec

import "encoding/json"

type JSON struct{}

func (j *JSON) MarshalBinary(d any) (bytes []byte, err error) {
	return json.Marshal(d)
}

func (j *JSON) UnmarshalBinary(data []byte, d any) (err error) {
	return json.Unmarshal(data, d)
}
