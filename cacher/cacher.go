package cacher // import "go.jamie.mt/mjcache/cacher"

import (
	"context"
	"time"
)

const (
	// KeepTTL is a constant when provided as the TTL, no expiration is applied
	// to the key/value pair.
	KeepTTL = -1

	// DefaultTTL is just a duration in this case, there shouldn't be any enforcement
	// in a cache implementation that falls back to this TTL.
	DefaultTTL = 24 * time.Hour
)

// Cacher defines an interface that our cache implementations have a basis off.
type Cacher interface {
	// Bytes retrieves the key if it exists, if a key does not exist in our cache
	// it's important an error is not returned but the returned `bytes` is nil.
	Bytes(ctx context.Context, key string) (bytes []byte, err error)

	// BytesMany retrieves many keys if they exists, it's important that the data type for these keys
	// is either a `string` or `[]byte` otherwise it'll append errors with unsupported type.
	BytesMany(ctx context.Context, keys ...string) (result map[string][]byte, err error)

	// Set attempts to set the key / value based on the bytes provided. If the passed `duration`
	// is <=0 the key will be stored without expiration (permanent set) otherwise
	// a TTL is applied.
	Set(ctx context.Context, key string, bytes []byte, duration time.Duration) (err error)

	// SetMany sets the key / value based on the bytes provided.
	SetMany(ctx context.Context, items map[string][]byte) (err error)

	// SetManyExp functions the same as SetMany but it applies the same expiry logic as Set.
	SetManyExp(ctx context.Context, items map[string][]byte, ttl time.Duration) (err error)

	// Exists checks if a cache key exists, if a key does not exist in our cache
	// it's important an error is not returned.
	Exists(ctx context.Context, key string) (has bool, err error)

	// Delete deletes a key if it exists, an error should only be returned in-case there's a io issue.
	Delete(ctx context.Context, key string) (err error)

	// DeleteMany deletes multiple keys if the exists, an error should only
	// be returned in-case there's a io issue.
	DeleteMany(ctx context.Context, keys ...string) (err error)

	// Close is responsible for clearing the allocated memory (if it's memory)
	// or closes the opened connection that handles the distributed actions.
	Close() (err error)
}
