package models

import (
	"github.com/google/uuid"
)

type CacheEvict struct {
	Identity uuid.UUID `json:"identity"`
	Keys     []string  `json:"keys"`
}
