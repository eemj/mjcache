package ristretto

import (
	"context"
	"fmt"
	"time"

	"github.com/dgraph-io/ristretto"
	cache "go.jamie.mt/mjcache/cacher"
)

type RistrettoCacher struct {
	cache *ristretto.Cache
}

// Bytes implements cacher.Cacher
func (m *RistrettoCacher) Bytes(ctx context.Context, key string) (bytes []byte, err error) {
	value, ok := m.cache.Get(key)
	if ok {
		bytes, ok := value.([]byte)
		if ok {
			return bytes, nil
		}
		return nil, fmt.Errorf("unexpected type '%T'", value)
	}
	return nil, nil
}

// BytesMany implements cacher.Cacher
func (m *RistrettoCacher) BytesMany(ctx context.Context, keys ...string) (map[string][]byte, error) {
	result := make(map[string][]byte)
	for _, key := range keys {
		bytes, err := m.Bytes(ctx, key)
		if err != nil {
			return nil, err
		}
		if bytes == nil {
			continue
		}
		result[key] = bytes
	}
	return result, nil
}

// Close implements cacher.Cacher
func (m *RistrettoCacher) Close() (err error) {
	m.cache.Close()
	return nil
}

// Delete implements cacher.Cacher
func (m *RistrettoCacher) Delete(ctx context.Context, key string) (err error) {
	m.cache.Del(key)
	return nil
}

// DeleteMany implements cacher.Cacher
func (m *RistrettoCacher) DeleteMany(ctx context.Context, keys ...string) error {
	for _, key := range keys {
		if err := m.Delete(ctx, key); err != nil {
			return err
		}
	}
	return nil
}

// Exists implements cacher.Cacher
func (m *RistrettoCacher) Exists(ctx context.Context, key string) (bool, error) {
	bytes, err := m.Bytes(ctx, key)
	if err != nil {
		return false, err
	}
	return bytes != nil, nil
}

// Set implements cacher.Cacher
func (m *RistrettoCacher) Set(ctx context.Context, key string, bytes []byte, duration time.Duration) (err error) {
	m.cache.SetWithTTL(key, bytes, 0, duration)
	return nil
}

// SetMany implements cacher.Cacher
func (m *RistrettoCacher) SetMany(ctx context.Context, items map[string][]byte) (err error) {
	return m.SetManyExp(ctx, items, 0)
}

// SetManyExp implements cacher.Cacher
func (m *RistrettoCacher) SetManyExp(ctx context.Context, items map[string][]byte, ttl time.Duration) (err error) {
	for key, value := range items {
		err = m.Set(ctx, key, value, ttl)
		if err != nil {
			return err
		}
	}
	return nil
}

var _ cache.Cacher = &RistrettoCacher{}

type Config struct {
	NumCounters int64
	MaxCost     int64
	BufferItems int64
}

func NewRistretto(config Config) (*RistrettoCacher, error) {
	cache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: config.NumCounters,
		MaxCost:     config.MaxCost,
		BufferItems: config.BufferItems,
	})
	if err != nil {
		return nil, err
	}
	return &RistrettoCacher{
		cache: cache,
	}, nil
}
