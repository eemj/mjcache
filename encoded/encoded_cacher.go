package encoded // import "go.jamie.mt/mjcache/encoded"

import (
	"context"
	"time"

	"go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
)

type EncodedCacher struct {
	cacher cacher.Cacher
	codec  codec.Codec
}

func (c *EncodedCacher) Close() error { return c.cacher.Close() }

func (c *EncodedCacher) Delete(ctx context.Context, key string) error {
	return c.cacher.Delete(ctx, key)
}

func (c *EncodedCacher) DeleteMany(ctx context.Context, keys ...string) error {
	return c.cacher.DeleteMany(ctx, keys...)
}

func (c *EncodedCacher) Exists(ctx context.Context, key string) (bool, error) {
	return c.cacher.Exists(ctx, key)
}

func NewEncodedCacher(cacher cacher.Cacher, codec codec.Codec) *EncodedCacher {
	return &EncodedCacher{
		cacher: cacher,
		codec:  codec,
	}
}

func Set[T any](
	ctx context.Context,
	c *EncodedCacher,
	key string,
	value T,
	duration time.Duration,
) (err error) {
	bytes, err := c.codec.MarshalBinary(value)
	if err != nil {
		return err
	}
	return c.cacher.Set(ctx, key, bytes, duration)
}

func SetMany[T any](
	ctx context.Context,
	c *EncodedCacher,
	items map[string]T,
) (err error) {
	_items := make(map[string][]byte, len(items))
	for key, value := range items {
		bytes, err := c.codec.MarshalBinary(value)
		if err != nil {
			return err
		}
		if len(bytes) == 0 {
			continue
		}
		_items[key] = bytes
	}
	return c.cacher.SetMany(ctx, _items)
}

func SetManyExp[T any](
	ctx context.Context,
	c *EncodedCacher,
	items map[string]T,
	duration time.Duration,
) (err error) {
	_items := make(map[string][]byte, len(items))
	for key, value := range items {
		bytes, err := c.codec.MarshalBinary(value)
		if err != nil {
			return err
		}
		if len(bytes) == 0 {
			continue
		}
		_items[key] = bytes
	}
	return c.cacher.SetManyExp(ctx, _items, duration)
}

func Get[T any](ctx context.Context, c *EncodedCacher, key string) (t T, err error) {
	bytes, err := c.cacher.Bytes(ctx, key)
	if err != nil {
		return t, err
	}
	if len(bytes) == 0 {
		return t, nil
	}
	err = c.codec.UnmarshalBinary(bytes, &t)
	if err != nil {
		return t, err
	}
	return t, nil
}

func GetMany[T any](ctx context.Context, c *EncodedCacher, keys ...string) (map[string]T, error) {
	items, err := c.cacher.BytesMany(ctx, keys...)
	if err != nil {
		return nil, err
	}
	result := make(map[string]T, len(items))
	for key, bytes := range items {
		if len(bytes) == 0 {
			continue
		}
		var value T
		err = c.codec.UnmarshalBinary(bytes, &value)
		if err != nil {
			return nil, err
		}
		result[key] = value
	}
	return result, nil
}
