package hybrid // import "go.jamie.mt/mjcache/hybrid"

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/google/uuid"
	goredis "github.com/redis/go-redis/v9"
	cache "go.jamie.mt/mjcache/cacher"
	"go.jamie.mt/mjcache/codec"
	"go.jamie.mt/mjcache/internal/models"
	"go.jamie.mt/mjcache/redis"
)

var (
	ErrLocalCacheIsNotInitialised = errors.New("local cache is not initialised")
)

type HybridEvictCallback func(keys ...string)

type HybridOptions struct {
	Topic         string
	RedisCacher   *redis.RedisCacher
	LocalCache    cache.Cacher
	EvictCallback HybridEvictCallback
}

type HybridCacher struct {
	codec codec.Codec

	redis *redis.RedisCacher
	local cache.Cacher

	topic    string
	identity uuid.UUID

	pubsub         *goredis.PubSub
	subscribeStopc chan struct{}
	subscribeErrc  chan error
	subscribeWg    sync.WaitGroup

	evictCallback HybridEvictCallback
}

var _ cache.Cacher = &HybridCacher{}

func NewHybridCacher(config HybridOptions) (*HybridCacher, error) {
	if config.LocalCache == nil {
		return nil, ErrLocalCacheIsNotInitialised
	}

	hybrid := &HybridCacher{
		identity: uuid.New(),
		codec:    &codec.MsgPack{},

		subscribeStopc: make(chan struct{}),
		subscribeErrc:  make(chan error),

		topic: config.Topic,
		redis: config.RedisCacher,
		local: config.LocalCache,

		evictCallback: config.EvictCallback,
	}

	ctx := context.Background()

	err := hybrid.redis.Instance().Ping(ctx).Err()
	if err != nil {
		return nil, err
	}

	hybrid.pubsub = hybrid.redis.Instance().Subscribe(ctx, hybrid.topic)

	go hybrid.subscribe()

	return hybrid, nil
}

func (c *HybridCacher) Identity() uuid.UUID { return c.identity }

func (c *HybridCacher) subscribe() {
	c.subscribeWg.Add(1)
	defer c.subscribeWg.Done()

	ch := c.pubsub.Channel()

	for {
		select {
		case msg := <-ch:
			if msg == nil {
				break
			}

			var (
				cacheEvict models.CacheEvict
				err        = c.codec.UnmarshalBinary([]byte(msg.Payload), &cacheEvict)
			)

			if err != nil {
				select {
				case c.subscribeErrc <- err:
				default:
				}
			}

			// We did the action already, no point doing it again.
			if cacheEvict.Identity == c.identity {
				break
			}

			// Callback, to be used for debugging by the callers.
			if c.evictCallback != nil {
				c.evictCallback(cacheEvict.Keys...)
			}

			for _, key := range cacheEvict.Keys {

				err := c.local.Delete(context.TODO(), key)
				if err != nil {
					select {
					case c.subscribeErrc <- err:
					default:
					}
				}
			}
		case <-c.subscribeStopc:
			return
		}
	}
}

func (c *HybridCacher) Errors() <-chan error {
	return c.subscribeErrc
}

func (c *HybridCacher) Bytes(
	ctx context.Context,
	key string,
) (bytes []byte, err error) {
	bytes, err = c.local.Bytes(ctx, key)
	if err != nil {
		return nil, err
	}
	if bytes != nil {
		return bytes, nil
	}

	var (
		remaining time.Duration
		startTime = time.Now()
	)

	// Initialize a pipeline to send the commands in one one request rather than
	// separate requests.
	var (
		stringCmd   *goredis.StringCmd
		durationCmd *goredis.DurationCmd
	)
	_, err = c.redis.Instance().Pipelined(ctx, func(pipe goredis.Pipeliner) error {
		stringCmd = pipe.Get(ctx, key)
		durationCmd = pipe.PTTL(ctx, key)
		return nil
	})
	if err != nil {
		if errors.Is(err, goredis.Nil) {
			return nil, nil
		}
		return nil, err
	}
	bytes, _ = stringCmd.Bytes()
	remaining = durationCmd.Val()
	if remaining != goredis.KeepTTL {
		remaining -= time.Since(startTime)

		// If the key is about to expired, don't bother setting it in the
		// local cache.
		if remaining < 0 {
			return bytes, nil
		}
	} else {
		remaining = cache.KeepTTL
	}

	// Store the bytes in our local cache with the calculated remaining time.
	err = c.local.Set(ctx, key, bytes, remaining)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

// Delete implements cache.Cacher
func (c *HybridCacher) Delete(
	ctx context.Context,
	key string,
) error {
	if err := errors.Join(
		c.redis.Delete(ctx, key),
		c.local.Delete(ctx, key),
	); err != nil {
		return err
	}

	if err := c.publishEvict(ctx, key); err != nil {
		return err
	}

	return nil
}

// Exists implements cache.Cacher
func (c *HybridCacher) Exists(
	ctx context.Context,
	key string,
) (bool, error) {
	has, err := c.local.Exists(ctx, key)
	if err != nil {
		return false, err
	}
	if has {
		return has, nil
	}
	return c.redis.Exists(ctx, key)
}

// Set implements cache.Cacher
func (c *HybridCacher) Set(
	ctx context.Context,
	key string,
	bytes []byte,
	duration time.Duration,
) error {
	if err := errors.Join(
		c.redis.Set(ctx, key, bytes, duration),
		c.local.Set(ctx, key, bytes, duration),
	); err != nil {
		return err
	}

	if err := c.publishEvict(ctx, key); err != nil {
		return err
	}

	return nil
}

// Close implements cache.Cacher
func (c *HybridCacher) Close() error {
	// Stop the subscriber routine.
	c.subscribeStopc <- struct{}{}

	// Close the pubsub connection.
	if c.pubsub != nil {
		if err := c.pubsub.Close(); err != nil {
			return err
		}
	}

	// Close the redis connection.
	if c.redis != nil {
		if err := c.redis.Close(); err != nil {
			return err
		}
	}

	// Close the subscription error channel.
	close(c.subscribeErrc)

	// Wait for the subscriber routine to terminate.
	c.subscribeWg.Wait()

	return nil
}

// BytesMany implements cache.Cacher
func (c *HybridCacher) BytesMany(ctx context.Context, keys ...string) (result map[string][]byte, err error) {
	result, err = c.local.BytesMany(ctx, keys...)
	if err != nil {
		return nil, err
	}
	resultLength := len(result)
	keysLength := len(keys)
	if resultLength == keysLength {
		return result, nil
	}

	missing := make([]string, 0, (keysLength - resultLength))
	for _, key := range keys {
		_, exists := result[key]
		if !exists {
			missing = append(missing, key)

		}
	}
	if len(missing) == 0 {
		return result, nil
	}
	// Attempt to retrieve it from redis
	newResult, err := c.redis.BytesMany(ctx, missing...)
	if err != nil {
		return nil, err
	}
	// Store the new result in our local cache
	if err = c.local.SetMany(ctx, newResult); err != nil {
		return nil, err
	}
	// Merge the old & new
	for key, value := range newResult {
		result[key] = value
	}

	return result, nil
}

// DeleteMany implements cache.Cacher
func (c *HybridCacher) DeleteMany(ctx context.Context, keys ...string) (err error) {
	if err := errors.Join(
		c.redis.DeleteMany(ctx, keys...),
		c.local.DeleteMany(ctx, keys...),
	); err != nil {
		return err
	}

	return c.publishEvict(ctx, keys...)
}

// SetMany implements cache.Cacher
func (c *HybridCacher) SetMany(ctx context.Context, items map[string][]byte) (err error) {
	if err := errors.Join(
		c.redis.SetMany(ctx, items),
		c.local.SetMany(ctx, items),
	); err != nil {
		return err
	}

	var keys []string
	for key := range items {
		keys = append(keys, key)
	}

	if err := c.publishEvict(ctx, keys...); err != nil {
		return err
	}

	return nil
}

// SetManyExp implements cache.Cacher
func (c *HybridCacher) SetManyExp(ctx context.Context, items map[string][]byte, duration time.Duration) (err error) {
	if err := errors.Join(
		c.redis.SetManyExp(ctx, items, duration),
		c.local.SetManyExp(ctx, items, duration),
	); err != nil {
		return err
	}

	var keys []string
	for key := range items {
		keys = append(keys, key)
	}

	if err := c.publishEvict(ctx, keys...); err != nil {
		return err
	}

	return nil
}

func (c *HybridCacher) Redis() *redis.RedisCacher { return c.redis }

func (c *HybridCacher) Local() cache.Cacher { return c.local }

func (c *HybridCacher) publishEvict(ctx context.Context, keys ...string) (err error) {
	bytes, err := c.codec.MarshalBinary(models.CacheEvict{
		Identity: c.identity,
		Keys:     keys,
	})
	if err != nil {
		return err
	}
	return c.redis.Instance().Publish(ctx, c.topic, bytes).Err()
}
